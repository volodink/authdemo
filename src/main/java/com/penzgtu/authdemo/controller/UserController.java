package com.penzgtu.authdemo.controller;

import com.penzgtu.authdemo.entity.UserEntity;
import com.penzgtu.authdemo.exception.UserAlreadyExistsException;
import com.penzgtu.authdemo.exception.UserNotFoundException;
import com.penzgtu.authdemo.model.User;
import com.penzgtu.authdemo.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("")
    public List<User> getUsers() {
        return userService.getUsers();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUserById(@PathVariable Long id) {
        try {
            return ResponseEntity.ok(userService.getUserById(id));
        } catch (UserNotFoundException exception) {
            return ResponseEntity.badRequest().body(exception.getMessage());
        } catch (Exception exception) {
            return ResponseEntity.badRequest().body("Extremely bad ERROR happened! :(");
        }
    }

    @PostMapping
    public ResponseEntity<?> registerUser(@RequestBody UserEntity user) {
        try {
            userService.registerUser(user);
            return ResponseEntity.ok("User created successfully");

        } catch (UserAlreadyExistsException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception exception) {
            return ResponseEntity.badRequest().body("Extremely bad ERROR happened! :(");
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable Long id) {
        try {
            return ResponseEntity.ok(userService.deleteUser(id));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Extremely bad ERROR happened! :(");

        }
    }
}
