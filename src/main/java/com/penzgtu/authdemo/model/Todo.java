package com.penzgtu.authdemo.model;

import com.penzgtu.authdemo.entity.TodoEntity;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@Getter
@Setter
public class Todo {
    private Long id;
    private String title;
    private String description;
    private Boolean isDone;

    public static Todo toModel(TodoEntity entity) {
        Todo model = new Todo();
        model.setId(entity.getId());
        model.setTitle(entity.getTitle());
        model.setDescription(entity.getDescription());
        model.setIsDone(entity.getDone());
        return model;
    }
}
