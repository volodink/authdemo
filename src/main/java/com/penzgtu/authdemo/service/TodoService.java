package com.penzgtu.authdemo.service;

import com.penzgtu.authdemo.entity.TodoEntity;
import com.penzgtu.authdemo.exception.TodoNotFoundException;
import com.penzgtu.authdemo.exception.UserNotFoundException;
import com.penzgtu.authdemo.model.Todo;
import com.penzgtu.authdemo.repository.TodoRepository;
import com.penzgtu.authdemo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TodoService {

    private final TodoRepository todoRepository;
    private final UserRepository userRepository;

    @Autowired
    public TodoService(TodoRepository todoRepository, UserRepository userRepository) {
        this.todoRepository = todoRepository;
        this.userRepository = userRepository;
    }

    public Todo createTodo(TodoEntity todo, Long userId) throws UserNotFoundException {
        var user = userRepository.findById(userId);
        if (user.isEmpty())
            throw new UserNotFoundException("User not found");

        todo.setUser(user.get());

        return Todo.toModel(todoRepository.save(todo));
    }

    public Todo completeTodo(Long id) throws TodoNotFoundException {
        var todo = todoRepository.findById(id);

        if (todo.isEmpty()) {
            throw new TodoNotFoundException("Todo not found");
        }

        TodoEntity todoEntity = todo.get();
        todoEntity.setDone(!todoEntity.getDone());

        return Todo.toModel(todoRepository.save(todoEntity));
    }
}
