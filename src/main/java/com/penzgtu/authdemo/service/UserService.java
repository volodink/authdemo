package com.penzgtu.authdemo.service;

import com.penzgtu.authdemo.entity.UserEntity;
import com.penzgtu.authdemo.exception.UserAlreadyExistsException;
import com.penzgtu.authdemo.exception.UserNotFoundException;
import com.penzgtu.authdemo.model.User;
import com.penzgtu.authdemo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void registerUser(UserEntity user) throws UserAlreadyExistsException {
        var usr = userRepository.findByUserName(user.getUserName());

        if (usr.isPresent()) {
            throw new UserAlreadyExistsException("User already exists");
        }

        userRepository.save(user);
    }

    public List<User> getUsers() {
        List<UserEntity> users = userRepository.findAll().stream().toList();
        return users.stream().map(User::toModel).toList();
    }

    public User getUserById(Long id) throws UserNotFoundException {
        var user = userRepository.findById(id);

        if (user.isEmpty())
            throw new UserNotFoundException("User not found");

        return User.toModel(user.get());
    }

    public Long deleteUser(Long id) {
        userRepository.deleteById(id);
        return id;
    }
}
