create table "authdb-schema".user_entity
(
    id         bigserial
        primary key,
    first_name varchar(255),
    last_name  varchar(255),
    user_name  varchar(255),
    password   varchar(255)
);
