create table "authdb-schema".todo_entity
(
    id          bigserial
        primary key,
    description varchar(255),
    is_done     boolean,
    title       varchar(255),
    user_id     bigint
        constraint fkf9ejdcjqxblhqgf1kgkuyry9s
            references "authdb-schema".user_entity
);
