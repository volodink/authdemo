INSERT INTO "authdb-schema".user_entity (first_name, last_name, user_name, "password")
values ('Иванов', 'Иван', 'ivan42', '1');
INSERT INTO "authdb-schema".user_entity (first_name, last_name, user_name, "password")
values ('Иванов', 'Сергей', 'seriytovarish123', '2');

INSERT INTO "authdb-schema".todo_entity (description, is_done, title, user_id)
values ('Расширенное описание', false, 'Краткое описание задачи', 1);
INSERT INTO "authdb-schema".todo_entity (description, is_done, title, user_id)
values ('Расширенное описание', true, 'Краткое описание задачи', 2);
